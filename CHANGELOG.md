
0.12.0 / 2024-04-12
==================

- Add marches in menu
- Add redirect between apps + change app logo in topbar (transparent)
- Add scale
- remove mitt dependency
- Use ThemeStore instead of ThemeService
- Infinite delay for accepting membership request toast
- Correct profilPage css
- Add custom toast for approve site membership request
- Correct toast message approve site membership request

**Update Dependencies**

- Update caddy to 2.7
- Update libvue 0.27.3
- Update primevue 3.49.1 + update component
- Update packages (vite, vitest, sass prettier, ...)
- use nodejs 20.9
- update vite - CVE-2024-23331

0.11.0 / 2024-01-17
==================

- Manage request membership for site
- Sentry: send only tracing data to alfresco service
- Correct TopBar menu toggle button + correct css for starting page
- Add change theme
- update dependencies

0.10.1 / 2023-11-15
==================

- FIX: Don't use ACS_USERNAME to check if we are auth.
- Correct menu TopBar on mobile device

0.10.0 / 2023-11-06
==================

**New Features and Enhancements:**

- Sentry Configuration
- Add target to return from login routing
- PdfPage: add getDocument loadingTask
- AppSubmenu: port corrections
- Keycloak: Update authentication flow
- Use new translation service
- Use vite JS
- rewriting PreviewPage
- remove unsued emit
- AppTopBar: add switch to hide some buttons
- Error page
- Sharelink consultation page
- Correction limit on suppress all

**Fixed bugs:**

- Keycloak: wait user fully validate
- Rooter: fix error getPerson not routing to login page
- LoginPage: Correct router when login routing
- i18n: silent warn
- Correct css preview page mobile

**Update Dependencies**

- update libevue 0.23.4
- @vueuse/core: 10.5.0
- axios: 1.5.1
- pdfjs-dist: 3.11.174
- pinia: 2.1.6
- primevue: 3.35.0
- remixicon: 3.5.0
- vue-i18n: 9.4.1
- vue-router: 4.2.5
- vue3-observe-visibility: 0.1.2

## [0.9.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.8.3...0.9.0) (2023-07-27)

**New Features and Enhancements:**

- Custom OIDC configuration
- Move Toast bottom-right
- Use Remix icons
- Remove our patched Image component
- LoginPage: add link to ACA

**Fixed bugs:**

- Route error 404 to login page
- CorbeilleList: remove checkbox

**Update Dependencies**

- update pristy/pristy-libvue 0.17.4
- update vue-keycloak-js 2.4.0
- update core-js 3.31.1
- update mitt 3.0.1
- update pinia 2.1.4
- update primeflex 3.3.1
- update primevue 3.30.0
- update remixicon 3.4.0
- update vue-router 4.2.4
- Update saga-green theme

## [0.8.2](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.8.1...0.8.2) (2023-05-22)

**New Features and Enhancements:**

- Update login page

**Update Dependencies**

- update pinia 2.1.2
- update primevue 3.29.1
- update remixicon 3.3.0
- update vue-router 4.2.0

## [0.8.1](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.8.0...0.8.1) (2023-05-11)

**Fixed bugs:**

- remove ununsed import

**Update Dependencies**

- update pristy-libvue 0.16.5
- update axios 1.4.0
- update core-js 3.30.2
- update pinia 2.0.36
- update primevue 3.28.0

## [0.8.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.7.1...0.8.0) (2023-05-10)

**New Features and Enhancements:**

- Users management page
- limit deletion in trash when empty the trash
- add remixicon
- add lemondap configuration

**Update Dependencies**

- update @pristy/pristy-libvue 0.16.4

## [0.7.1](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.7.0...0.7.1) (2023-03-21)

**Fixed bugs:**

- Topbar tooltip in bottom

**Update Dependencies**

- update @pristy/pristy-libvue 0.14.2

## [0.7.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.6.0...0.7.0) (2023-03-20)

**New Features and Enhancements:**

- Global update on css
- Add buttons on profile page to "manage informations" and "change password" (from Keycloak)
- use Config Store (remove window.config)
- Add Keycloak logout

**Fixed bugs:**

- Correction on login page
- Add error message if id or password empty
- add redirect on errors without status

**Update Dependencies**

- update @pristy/pristy-libvue 0.13.3
- update core-js 3.29.1
- update pinia 2.0.33
- update primevue to 3.25.0
- Update axios to 1.2

## [0.6.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.5.0...0.6.0) (2023-01-31)

**New Features and Enhancements:**

- Treanslation to english
- Add swith language in profile page
- Add search icon into topbar
- Add keycloak-js to permit OIDC authentication
- Add error page when Alfresco is not started
- Rework login page

**Update Dependencies**

- Use @pristy/pristy-libvue 0.9.0
- Update primevue to 3.22.1
- Update axios to 1.2.2
- Update vue to 3.2.45

## [0.5.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.4.2...0.5.0) (2022-12-22)

**New Features and Enhancements:**

- Ajout ComposantIcon dans la datatable de la corbeille
- Paramètre de présence des apps dans le env-config
- push dans le menu paramétré
- Updated icon management
- Ajout de la ProfilPage.vue
- Ajout de la corbeille dans le menu options

**Update Dependencies**

- Use @pristy/pristy-libvue 0.6.2
- Update primevue to 3.21.0


## [0.4.2](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.4.1...0.4.2) (2022-11-21)

**New Features and Enhancements:**

- Menu éléments tronqués si trop longs

**Update Dependencies**

- Use primevue 3.18.1


## [0.4.1](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.4.0...0.4.1) (2022-11-04)

- Update page title and logo

## [0.4.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.3.0...0.4.0) (2022-10-24)

**New Features and Enhancements:**

- Nouveau Parcours Utilisateur

**Update Dependencies**

Use pristy-libvue 0.3.1


## [0.3.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.2.0...0.3.0) (2022-10-12)

**New Features and Enhancements:**

- Ajout recherche et corbeille dans le menu  
- Affichage d’erreur de mot de passe

**Fixed bugs:**

- Annulation modification espace

**Update Dependencies**

- Use pristy-libevue 0.2


## [0.2.0](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.1.1...0.2.0) (2022-09-19)

**New Features and Enhancements:**

- Refonte du menu
- Popup confirmation suppression de tout les éléments de la corbeille
- Forcer la création espace acte en privé
- Formulaire création espace, id uniquement


## [0.1.1](https://gitlab.com/pristy-oss/pristy-portail/-/compare/0.1.0...0.1.1) (2022-08-31)

**Fixed bugs:**

- Correction d’un problème de routage


## 0.1.0 (2022-08-30)

- First public version
