/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import "intersection-observer";

const instances = new WeakMap();

function createObserver(el, vnode, modifiers, callback) {
  const observer = new IntersectionObserver((entries) => {
    const entry = entries[0];
    if (entry.isIntersecting) {
      callback();
      if (modifiers.once) {
        disconnectObserver(observer, el);
      }
    }
  });

  // Observe when element is inserted in DOM
  vnode.context.$nextTick(() => observer.observe(el));

  return observer;
}
function disconnectObserver(observer, el) {
  console.log("Disconnecting observer", el);
  if (observer) observer.disconnect();
}

function bind(el, { value, modifiers }, vnode) {
  console.log("Binding element", el);
  if (typeof window.IntersectionObserver === "undefined") {
    console.log("IntersectionObserver API is not available in your browser.");
  } else {
    const observer = createObserver(el, vnode, modifiers, () => {
      console.log("Element is visible", el);
      const callback = value;
      if (typeof callback === "function") callback();
    });
    instances.set(el, { observer });
  }
}

function update(el, { value, oldValue }, vnode) {
  if (value === oldValue) return;

  const { observer } = instances.get(el);
  disconnectObserver(observer, el);
  bind(el, { value }, vnode);
}

function unbind(el) {
  if (instances.has(el)) {
    const { observer } = instances.get(el);
    disconnectObserver(observer, el);
    instances.delete(el);
  }
}

export default {
  bind,
  update,
  unbind,
};
