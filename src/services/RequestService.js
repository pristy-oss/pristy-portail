/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { sitesApi } from "@pristy/pristy-libvue";

class RequestService {
  getRequests() {
    const opts = {
      skipCount: 0,
      maxItems: 100,
    };
    return sitesApi.getSiteMembershipRequests(opts).then((data) => {
      return data.list.entries.map((data) => data.entry);
    });
  }
  refuseRequest(siteId, inviteeId) {
    const opts = {};

    return sitesApi
      .rejectSiteMembershipRequest(siteId, inviteeId, opts)
      .then((data) => {
        return data;
      });
  }
  approveRequest(siteId, inviteeId) {
    const opts = {};

    return sitesApi
      .approveSiteMembershipRequest(siteId, inviteeId, opts)
      .then((data) => {
        return data;
      });
  }
}

export default new RequestService();
